#!/bin/bash -x

repo_url=git@gitlab.com:filipg/test-area5.git
storage_dir=/home/filipg/storage15
work_dir=test-area5

git --version
git-annex version

mkdir $work_dir
cd $work_dir
git init
git annex init
git remote add origin $repo_url
echo 'Hello world!' > test.txt
git add test.txt
dd if=/dev/urandom of=test.bin bs=1 count=1000000
git annex add test.bin
git commit -m 'init'
git push origin master

mkdir -p $storage_dir
git annex initremote shared-storage type=directory directory=$storage_dir encryption=none
ls -l $storage_dir
git annex copy test.bin --to shared-storage
ls -l $storage_dir

cd ..

git clone $repo_url ${work_dir}-copy
cd ${work_dir}-copy
wc -c test.bin

git annex init
git annex initremote shared-storage type=directory directory=$storage_dir encryption=none
ls -l $storage_dir
git annex get test.bin --from shared-storage
wc -c test.bin
